package com.deyneka.model;

import com.deyneka.model.Annotation.Column;
import com.deyneka.model.Annotation.PrimaryKey;
import com.deyneka.model.Annotation.Table;

@Table(name = "country")
public class CountryEntity {
    @PrimaryKey
    @Column(name = "id", length = 5)
    private String id;
    @Column(name = "name", length = 15)
    private String name;

    public CountryEntity() {
    }

    public CountryEntity(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return String.format("%-5s %-15s %s", id, name);
    }
}
