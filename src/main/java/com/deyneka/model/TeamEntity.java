package com.deyneka.model;

import com.deyneka.model.Annotation.Column;
import com.deyneka.model.Annotation.PrimaryKey;
import com.deyneka.model.Annotation.Table;

@Table(name = "project")
public class TeamEntity {
    @PrimaryKey
    @Column(name = "id", length = 10)
    private String id;
    @Column(name = "name", length = 45)
    private String name;
    @Column(name = "win")
    private Integer win;
    @Column(name = "loss")
    private Integer loss;
    @Column(name = "rating")
    private Integer rating;
    @Column(name = "country_id")
    private Integer country_id;

    public TeamEntity() {
    }

    public TeamEntity(String id, String name, Integer win, Integer loss, Integer rating, Integer country_id) {
        this.id = id;
        this.name = name;
        this.win = win;
        this.loss = loss;
        this.rating = rating;
        this.country_id = country_id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getWin() {
        return win;
    }

    public void setWin(Integer win) {
        this.win = win;
    }

    public Integer getLoss() {
        return loss;
    }

    public void setLoss(Integer loss) {
        this.loss = loss;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public Integer getCountry_id() {
        return country_id;
    }

    public void setCountry_id(Integer country_id) {
        this.country_id = country_id;
    }

    @Override
    public String toString() {
        return String.format("%-11s %-15s %-5d %-5d %-15d %d", id, name, win, loss, rating, country_id);
    }
}
