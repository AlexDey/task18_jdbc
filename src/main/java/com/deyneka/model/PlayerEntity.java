package com.deyneka.model;

import com.deyneka.model.Annotation.Column;
import com.deyneka.model.Annotation.PrimaryKey;
import com.deyneka.model.Annotation.Table;

@Table(name = "employee")
public class PlayerEntity {
    @PrimaryKey
    @Column(name = "id")
    private Integer id;
    @Column(name = "full_name",length = 45)
    private String full_name;
    @Column(name = "year_of_birth",length = 4)
    private String year_of_birth;
    @Column(name = "uniform_id", length = 5)
    private String uniform_id;

    public PlayerEntity() {
    }

    public PlayerEntity(Integer id, String full_name, String year_of_birth, String uniform_id) {
        this.id = id;
        this.full_name = full_name;
        this.year_of_birth = year_of_birth;
        this.uniform_id = uniform_id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer empNo) {
        this.id = id;
    }

    public String getFullName() {
        return full_name;
    }

    public void setFullName(String full_name) {
        this.full_name = full_name;
    }

    public String getYearOfBirth() {
        return year_of_birth;
    }

    public void setYearOfBirth(String year_of_birth) {
        this.year_of_birth = year_of_birth;
    }

    public String getUniformId() {
        return uniform_id;
    }

    public void setUniformId(String deptNo) {
        this.uniform_id = uniform_id;
    }

    @Override
    public String toString() {
        return String.format("%-7d %-15s %-15s %s", id, full_name, year_of_birth, uniform_id);
    }
}
