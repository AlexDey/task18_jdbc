package com.deyneka.model;

import com.deyneka.model.Annotation.Column;

public class TeamHasPlayer {
    @Column(name = "team_id")
    private Integer team_id;
    @Column(name = "player_id", length = 10)
    private Integer player_id;

    public TeamHasPlayer() {
    }

    public TeamHasPlayer(Integer team_id, Integer player_id) {
        this.team_id = team_id;
        this.player_id = player_id;
    }

    public Integer getTeam_id() {
        return team_id;
    }

    public void setTeam_id(Integer team_id) {
        this.team_id = team_id;
    }

    public Integer getPlayer_id() {
        return player_id;
    }

    public void setPlayer_id(Integer player_id) {
        this.player_id = player_id;
    }

    @Override
    public String toString() {
        return String.format("%-5d %-5d", getTeam_id(), getPlayer_id());
    }
}
