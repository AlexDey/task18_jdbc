package com.deyneka.service;

import com.deyneka.DAO.implementation.CountryDaoImpl;
import com.deyneka.DAO.implementation.PlayerDaoImpl;
import com.deyneka.model.CountryEntity;
import com.deyneka.model.PlayerEntity;
import com.deyneka.persistant.ConnectionManager;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public class CountryService {

    public List<CountryEntity> findAll() throws SQLException {
        return new CountryDaoImpl().findAll();
    }

    public CountryEntity findById(String id) throws SQLException {
        return new CountryDaoImpl().findById(id);
    }

    public int create(CountryEntity entity) throws SQLException {
        return new CountryDaoImpl().create(entity);
    }

    public int update(CountryEntity entity) throws SQLException {
        return new CountryDaoImpl().update(entity);
    }

    public int delete(String id) throws SQLException {
        return new CountryDaoImpl().delete(id);
    }

}
