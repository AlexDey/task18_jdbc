package com.deyneka.service;

import com.deyneka.DAO.implementation.PlayerDaoImpl;
import com.deyneka.model.PlayerEntity;

import java.sql.SQLException;
import java.util.List;

public class PlayerService {
    public List<PlayerEntity> findAll() throws SQLException {
        return new PlayerDaoImpl().findAll();
    }

    public PlayerEntity findById(Integer id) throws SQLException {
        return new PlayerDaoImpl().findById(id);
    }

    public int create(PlayerEntity entity) throws SQLException {
        return new PlayerDaoImpl().create(entity);
    }

    public int update(PlayerEntity entity) throws SQLException {
        return new PlayerDaoImpl().update(entity);
    }

    public int delete(Integer id) throws SQLException {
        return new PlayerDaoImpl().delete(id);
    }

    public List<PlayerEntity> findByName(String name) throws SQLException {
        return new PlayerDaoImpl().findByName(name);
    }
}
