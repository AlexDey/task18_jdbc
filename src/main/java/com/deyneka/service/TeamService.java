package com.deyneka.service;

import com.deyneka.DAO.implementation.TeamDaoImpl;
import com.deyneka.model.TeamEntity;
import java.sql.SQLException;
import java.util.List;

public class TeamService {
    public List<TeamEntity> findAll() throws SQLException {
        return new TeamDaoImpl().findAll();
    }

   public TeamEntity findById(String id) throws SQLException{
       return new TeamDaoImpl().findById(id);
    }

    public int create(TeamEntity entity) throws SQLException{
        return new TeamDaoImpl().create(entity);
    }

    public int update(TeamEntity entity) throws SQLException{
        return new TeamDaoImpl().update(entity);
    }

    public int delete(String id) throws SQLException{
        return new TeamDaoImpl().delete(id);
    }
}
