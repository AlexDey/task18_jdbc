package com.deyneka.service;

import com.deyneka.DAO.implementation.TeamHasPlayerDaoImpl;
import com.deyneka.model.TeamHasPlayer;

import java.sql.SQLException;
import java.util.List;

public class TeamHasPlayerService {
    public List<com.deyneka.model.TeamHasPlayer> findAll() throws SQLException {
        return new TeamHasPlayerDaoImpl().findAll();
    }

//   public List<TeamHasPlayerService> findById(ID id) throws SQLException{
//
//    };

    public int create(com.deyneka.model.TeamHasPlayer entity) throws SQLException{
        return new TeamHasPlayerDaoImpl().create(entity);
    }

    public int update(com.deyneka.model.TeamHasPlayer entity) throws SQLException{
        return new TeamHasPlayerDaoImpl().update(entity);
    }

    public int delete(com.deyneka.model.TeamHasPlayer pk) throws SQLException{
        return new TeamHasPlayerDaoImpl().delete(pk);
    }
}
