package com.deyneka.DAO.implementation;

import com.deyneka.DAO.EmployeeDAO;
import com.deyneka.model.PlayerEntity;
import com.deyneka.persistant.ConnectionManager;
import com.deyneka.transformer.Transformer;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class PlayerDaoImpl implements EmployeeDAO {
    private static final String FIND_ALL = "SELECT * FROM player";
    private static final String DELETE = "DELETE FROM player WHERE id=?";
    private static final String CREATE = "INSERT player (id, full_name, year_of_birth, uniform_id) VALUES (?, ?, ?, ?)";
    private static final String UPDATE = "UPDATE player SET full_name=?, year_of_birth=?, uniform_id=? WHERE id=?";
    private static final String FIND_BY_ID = "SELECT * FROM player WHERE id=?";
    private static final String FIND_BY_FIRST_NAME = "SELECT * FROM player WHERE id=?";
    private static final String FIND_BY_DEPT_NO = "SELECT * FROM player WHERE uniform_id=?";

    @Override
    public List<PlayerEntity> findAll() throws SQLException {
        List<PlayerEntity> employees = new ArrayList<>();
        Connection connection = ConnectionManager.getConnection();
        try (Statement statement = connection.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(FIND_ALL)) {
                while (resultSet.next()) {
                    employees.add((PlayerEntity) new Transformer(PlayerEntity.class).fromResultSetToEntity(resultSet));
                }
            }
        }
        return employees;
    }

    @Override
    public PlayerEntity findById(Integer id) throws SQLException {
        PlayerEntity entity=null;
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(FIND_BY_ID)) {
            ps.setInt(1,id);
            try (ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                    entity=(PlayerEntity)new Transformer(PlayerEntity.class).fromResultSetToEntity(resultSet);
                    break;
                }
            }
        }
        return entity;
    }

    @Override
    public int create(PlayerEntity entity) throws SQLException {
        Connection conn = ConnectionManager.getConnection();
        try (PreparedStatement ps = conn.prepareStatement(CREATE)) {
            ps.setInt(1,entity.getId());
            ps.setString(2,entity.getFullName());
            ps.setString(3,entity.getYearOfBirth());
            ps.setString(4,entity.getUniformId());
            return ps.executeUpdate();
        }
    }

    @Override
    public int update(PlayerEntity entity) throws SQLException {
        Connection conn = ConnectionManager.getConnection();
        try (PreparedStatement ps = conn.prepareStatement(UPDATE)) {
            ps.setInt(1,entity.getId());
            ps.setString(2,entity.getFullName());
            ps.setString(3,entity.getYearOfBirth());
            ps.setString(4,entity.getUniformId());
            return ps.executeUpdate();
        }
    }

    @Override
    public int delete(Integer id) throws SQLException {
        Connection conn = ConnectionManager.getConnection();
        try (PreparedStatement ps = conn.prepareStatement(DELETE)) {
            ps.setInt(1,id);
            return ps.executeUpdate();
        }
    }

    @Override
    public List<PlayerEntity> findByName(String name) throws SQLException {
        List<PlayerEntity> employees = new ArrayList<>();
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(FIND_BY_FIRST_NAME)) {
            ps.setString(1,name);
            try (ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                    employees.add((PlayerEntity) new Transformer(PlayerEntity.class).fromResultSetToEntity(resultSet));
                }
            }
        }
        return employees;
    }

    @Override
    public List<PlayerEntity> findByDeptNo(String deptNo) throws SQLException {
        List<PlayerEntity> employees = new ArrayList<>();
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(FIND_BY_DEPT_NO)) {
            ps.setString(1,deptNo);
            try (ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                    employees.add((PlayerEntity) new Transformer(PlayerEntity.class).fromResultSetToEntity(resultSet));
                }
            }
        }
        return employees;
    }
}
