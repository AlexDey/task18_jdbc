package com.deyneka.DAO.implementation;

import com.deyneka.DAO.ProjectDAO;
import com.deyneka.model.TeamEntity;
import com.deyneka.persistant.ConnectionManager;
import com.deyneka.transformer.Transformer;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class TeamDaoImpl implements ProjectDAO {
    private static final String FIND_ALL = "SELECT * FROM team";
    private static final String DELETE = "DELETE FROM team WHERE id=?";
    private static final String CREATE = "INSERT team (id, name, win) VALUES (?, ?, ?)";
    private static final String UPDATE = "UPDATE team SET name=?, win=? WHERE id=?";
    private static final String FIND_BY_ID = "SELECT * FROM team WHERE id=?";

    @Override
    public List<TeamEntity> findAll() throws SQLException {
        List<TeamEntity> projects = new ArrayList<>();
        Connection connection = ConnectionManager.getConnection();
        try (Statement statement = connection.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(FIND_ALL)) {
                while (resultSet.next()) {
                    projects.add((TeamEntity) new Transformer(TeamEntity.class).fromResultSetToEntity(resultSet));
                }
            }
        }
        return projects;
    }

    @Override
    public TeamEntity findById(String id) throws SQLException {
        TeamEntity entity=null;
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(FIND_BY_ID)) {
            ps.setString(1,id);
            try (ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                    entity=(TeamEntity)new Transformer(TeamEntity.class).fromResultSetToEntity(resultSet);
                    break;
                }
            }
        }
        return entity;
    }

    @Override
    public int create(TeamEntity entity) throws SQLException {
        Connection conn = ConnectionManager.getConnection();
        try (PreparedStatement ps = conn.prepareStatement(CREATE)) {
            ps.setString(1,entity.getId());
            ps.setString(2,entity.getName());
            ps.setInt(3,entity.getWin());
            ps.setInt(4,entity.getLoss());
            ps.setInt(5,entity.getRating());
            ps.setInt(6,entity.getCountry_id());
            return ps.executeUpdate();
        }
    }

    @Override
    public int update(TeamEntity entity) throws SQLException {
        Connection conn = ConnectionManager.getConnection();
        try (PreparedStatement ps = conn.prepareStatement(UPDATE)) {
            ps.setString(1,entity.getId());
            ps.setString(2,entity.getName());
            ps.setInt(3,entity.getWin());
            ps.setInt(4,entity.getLoss());
            ps.setInt(5,entity.getRating());
            ps.setInt(6,entity.getCountry_id());
            return ps.executeUpdate();
        }
    }

    @Override
    public int delete(String id) throws SQLException {
        Connection conn = ConnectionManager.getConnection();
        try (PreparedStatement ps = conn.prepareStatement(DELETE)) {
            ps.setString(1, id);
            return ps.executeUpdate();
        }
    }
}

