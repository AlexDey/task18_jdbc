package com.deyneka.DAO.implementation;

import com.deyneka.DAO.WorksOnDAO;
import com.deyneka.model.TeamHasPlayer;
import com.deyneka.persistant.ConnectionManager;
import com.deyneka.transformer.Transformer;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class TeamHasPlayerDaoImpl implements WorksOnDAO {
    private static final String FIND_ALL = "SELECT * FROM team_has_player";
    private static final String CREATE = "INSERT team_has_player (team_id, player_id) VALUES (?, ?)";

    @Override
    public List<TeamHasPlayer> findAll() throws SQLException {
        List<TeamHasPlayer> works = new ArrayList<>();
        Connection connection = ConnectionManager.getConnection();
        try (Statement statement = connection.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(FIND_ALL)) {
                while (resultSet.next()) {
                    works.add((TeamHasPlayer) new Transformer(TeamHasPlayer.class).fromResultSetToEntity(resultSet));
                }
            }
        }
        return works;
    }

    @Override
    public TeamHasPlayer findById(TeamHasPlayer pk_worksOn) throws SQLException {
        return null;
    }

    @Override
    public int create(TeamHasPlayer entity) throws SQLException {
        Connection conn = ConnectionManager.getConnection();
        try (PreparedStatement ps = conn.prepareStatement(CREATE)) {
            ps.setInt(1,entity.getPlayer_id());
            ps.setInt(2,entity.getTeam_id());
            return ps.executeUpdate();
        }
    }

    @Override
    public int update(TeamHasPlayer entity) throws SQLException {
        return 0;
    }

    @Override
    public int delete(TeamHasPlayer teamHasPlayer) throws SQLException {
        return 0;
    }

}
