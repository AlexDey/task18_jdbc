package com.deyneka.DAO.implementation;

import com.deyneka.DAO.DepartmentDAO;
import com.deyneka.model.CountryEntity;
import com.deyneka.persistant.ConnectionManager;
import com.deyneka.transformer.Transformer;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CountryDaoImpl implements DepartmentDAO {
    private static final String FIND_ALL = "SELECT * FROM country";
    private static final String DELETE = "DELETE FROM country WHERE id=?";
    private static final String CREATE = "INSERT country (id, name) VALUES (?, ?)";
    private static final String UPDATE = "UPDATE country SET id=? WHERE name=?";
    private static final String FIND_BY_ID = "SELECT * FROM country WHERE id=?";



    @Override
    public List<CountryEntity> findAll() throws SQLException {
        List<CountryEntity> departments = new ArrayList<>();
        Connection connection = ConnectionManager.getConnection();
        try (Statement statement = connection.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(FIND_ALL)) {
                while (resultSet.next()) {
                    departments.add((CountryEntity)new Transformer(CountryEntity.class).fromResultSetToEntity(resultSet));
                }
            }
        }
        return departments;
    }

    @Override
    public CountryEntity findById(String id) throws SQLException {
        CountryEntity entity=null;
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(FIND_BY_ID)) {
            ps.setString(1,id);
            try (ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                    entity=(CountryEntity)new Transformer(CountryEntity.class).fromResultSetToEntity(resultSet);
                    break;
                }
            }
        }
        return entity;
    }

    @Override
    public int create(CountryEntity entity) throws SQLException {
        Connection conn = ConnectionManager.getConnection();
        try (PreparedStatement ps = conn.prepareStatement(CREATE)) {
            ps.setString(1,entity.getId());
            ps.setString(2,entity.getName());
            return ps.executeUpdate();
        }
    }

    @Override
    public int update(CountryEntity entity) throws SQLException {
        Connection conn = ConnectionManager.getConnection();
        try (PreparedStatement ps = conn.prepareStatement(UPDATE)) {
            ps.setString(1,entity.getId());
            ps.setString(3,entity.getName());
            return ps.executeUpdate();
        }
    }

    @Override
    public int delete(String id) throws SQLException {
        Connection conn = ConnectionManager.getConnection();
        try (PreparedStatement ps = conn.prepareStatement(DELETE)) {
            ps.setString(1,id);
            return ps.executeUpdate();
        }
    }

}
