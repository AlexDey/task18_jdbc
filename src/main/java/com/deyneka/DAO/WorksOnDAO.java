package com.deyneka.DAO;

import com.deyneka.model.TeamHasPlayer;

public interface WorksOnDAO extends GeneralDAO<TeamHasPlayer, TeamHasPlayer> {
}
