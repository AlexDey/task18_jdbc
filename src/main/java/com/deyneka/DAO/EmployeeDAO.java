package com.deyneka.DAO;

import com.deyneka.model.PlayerEntity;

import java.sql.SQLException;
import java.util.List;

public interface EmployeeDAO extends GeneralDAO<PlayerEntity, Integer> {
    List<PlayerEntity> findByName(String name) throws SQLException;

    List<PlayerEntity> findByDeptNo(String deptNo) throws SQLException;
}
