package com.deyneka.DAO;

import com.deyneka.model.TeamEntity;

public interface ProjectDAO extends GeneralDAO<TeamEntity, String> {
}
