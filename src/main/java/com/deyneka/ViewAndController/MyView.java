package com.deyneka.ViewAndController;

import com.deyneka.model.*;
import com.deyneka.model.metadata.TableMetaData;
import com.deyneka.persistant.ConnectionManager;
import com.deyneka.service.*;
import com.deyneka.service.TeamHasPlayerService;

import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;
import java.util.*;

public class MyView {
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);

    public MyView() {
        menu = new LinkedHashMap<>();
        methodsMenu = new LinkedHashMap<>();
        menu.put("A", "   A - Select all table");
        menu.put("B", "   B - Select structure of DB");

        menu.put("1", "   1 - Table: Country");
        menu.put("11", "  11 - Create for Country");
        menu.put("12", "  12 - Update Country");
        menu.put("13", "  13 - Delete from Country");
        menu.put("14", "  14 - Select Country");
        menu.put("15", "  15 - Find Country by ID");
//        menu.put("16", "  16 - Delete from Country and move all employees to another department");

        menu.put("2", "   2 - Table: Player");
        menu.put("21", "  21 - Create for Player");
        menu.put("22", "  22 - Update Player");
        menu.put("23", "  23 - Delete from Player");
        menu.put("24", "  24 - Select Player");
        menu.put("25", "  25 - Find Player by ID");
        menu.put("26", "  26 - Find Player by Name");

        menu.put("3", "   3 - Table: Team");
        menu.put("31", "  31 - Create for Team");
        menu.put("32", "  32 - Update Team");
        menu.put("33", "  33 - Delete from Team");
        menu.put("34", "  34 - Select Team");
        menu.put("35", "  35 - Find Team by ID");

        menu.put("4", "   4 - Table: Team_has_player");
        menu.put("41", "  41 - Create for Team_has_player");
        menu.put("43", "  42 - Delete from Team_has_player");
        menu.put("44", "  43 - Select Team_has_player");

        menu.put("Q", "   Q - exit");

        methodsMenu.put("A", this::selectAllTable);
        methodsMenu.put("B", this::takeStructureOfDB);

        methodsMenu.put("11", this::createForCountry);
        methodsMenu.put("12", this::updateCountry);
        methodsMenu.put("13", this::deleteFromCountry);
        methodsMenu.put("14", this::selectCountry);
        methodsMenu.put("15", this::findCountryByID);

        methodsMenu.put("21", this::createForPlayer);
        methodsMenu.put("22", this::updatePlayer);
        methodsMenu.put("23", this::deleteFromPlayer);
        methodsMenu.put("24", this::selectPlayer);
        methodsMenu.put("25", this::findPlayerByID);
        methodsMenu.put("26", this::findPlayerByName);

        methodsMenu.put("31", this::createForTeam);
        methodsMenu.put("32", this::updateTeam);
        methodsMenu.put("33", this::deleteFromTeam);
        methodsMenu.put("34", this::selectTeam);
        methodsMenu.put("35", this::findTeamByID);

        methodsMenu.put("41", this::createForTeamHasPlayer);
        methodsMenu.put("42", this::deleteFromTeamHasPlayer);
        methodsMenu.put("43", this::selectTeamHasPlayer);
    }

    private void selectAllTable() throws SQLException {
        selectCountry();
        selectPlayer();
        selectTeam();
        selectTeamHasPlayer();
    }

    private void takeStructureOfDB() throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        MetaDataService metaDataService = new MetaDataService();
        List<TableMetaData> tables = metaDataService.getTablesStructure();
        System.out.println("TABLE OF DATABASE: " + connection.getCatalog());

        for (TableMetaData table : tables) {
            System.out.println(table);
        }
    }


    private void deleteFromCountry() throws SQLException {
        System.out.println("Input ID(id) for Country: ");
        String id = input.nextLine();
        CountryService departmentService = new CountryService();
        int count = departmentService.delete(id);
        System.out.printf("There are deleted %d rows\n", count);
    }

    private void createForCountry() throws SQLException {
        System.out.println("Input ID(id) for Country: ");
        String id = input.nextLine();
        System.out.println("Input name for Country: ");
        String name = input.nextLine();
        CountryEntity entity = new CountryEntity(id, name);

        CountryService departmentService = new CountryService();
        int count = departmentService.create(entity);
        System.out.printf("There are created %d rows\n", count);
    }

    private void updateCountry() throws SQLException {
        System.out.println("Input ID(id) for Country: ");
        String id = input.next();
        System.out.println("Input name for Country: ");
        String name = input.next();
        CountryEntity entity = new CountryEntity(id, name);

        CountryService departmentService = new CountryService();
        int count = departmentService.update(entity);
        System.out.printf("There are updated %d rows\n", count);
    }

    private void selectCountry() throws SQLException {
        System.out.println("\nTable: Country");
        CountryService departmentService = new CountryService();
        List<CountryEntity> departments = departmentService.findAll();
        for (CountryEntity entity : departments) {
            System.out.println(entity);
        }
    }

    private void findCountryByID() throws SQLException {
        System.out.println("Input ID(id) for Country: ");
        String id = input.nextLine();
        CountryService departmentService = new CountryService();
        CountryEntity entity = departmentService.findById(id);
        System.out.println(entity);
    }

    //------------------------------------------------------------------------

    private void deleteFromPlayer() throws SQLException {
        System.out.println("Input ID(id) for Player: ");
        Integer id = input.nextInt();
        input.nextLine();
        PlayerService employeeService = new PlayerService();
        int count = employeeService.delete(id);
        System.out.printf("There are deleted %d rows\n", count);
    }

    private void createForPlayer() throws SQLException {
        System.out.println("Input ID(id) for Player: ");
        Integer id = input.nextInt();
        input.nextLine();
        System.out.println("Input full name for Player: ");
        String empFirstName = input.nextLine();
        System.out.println("Input year of birth for Player: ");
        String empLastName = input.nextLine();
        System.out.println("Input uniform id for Player: ");
        String deptNo = input.nextLine();
        PlayerEntity entity = new PlayerEntity(id, empFirstName, empLastName, deptNo);
        PlayerService employeeService = new PlayerService();

        int count = employeeService.create(entity);
        System.out.printf("There are created %d rows\n", count);
    }

    private void updatePlayer() throws SQLException {
        System.out.println("Input ID(epm_no) for Employee: ");
        Integer id = input.nextInt();
        input.nextLine();
        System.out.println("Input emp_fmane for Employee: ");
        String empFirstName = input.nextLine();
        System.out.println("Input emp_lname for Employee: ");
        String empLastName = input.nextLine();
        System.out.println("Input dept_no for Employee: ");
        String deptNo = input.nextLine();
        PlayerEntity entity = new PlayerEntity(id, empFirstName, empLastName, deptNo);
        PlayerService employeeService = new PlayerService();

        int count = employeeService.update(entity);
        System.out.printf("There are updated %d rows\n", count);
    }

    private void selectPlayer() throws SQLException {
        System.out.println("\nTable: Employee");
        PlayerService employeeService = new PlayerService();
        List<PlayerEntity> employees = employeeService.findAll();
        for (PlayerEntity entity : employees) {
            System.out.println(entity);
        }
    }

    private void findPlayerByID() throws SQLException {
        System.out.println("Input ID(emp_no) for Employee: ");
        Integer id = input.nextInt();
        input.nextLine();
        PlayerService employeeService = new PlayerService();
        PlayerEntity entity = employeeService.findById(id);
        System.out.println(entity);
    }

    private void findPlayerByName() throws SQLException {
        System.out.println("Input First Name for Employee: ");
        String fname = input.nextLine();
        PlayerService employeeService = new PlayerService();
        List<PlayerEntity> employees = employeeService.findByName(fname);
        for (PlayerEntity entity : employees) {
            System.out.println(entity);
        }
    }

    //------------------------------------------------------------------------

    private void updateTeam() throws SQLException {
        System.out.println("Input ID(project_no) for Project: ");
        String id = input.nextLine();
        System.out.println("Input project_name for Project: ");
        String name = input.nextLine();
        System.out.println("Input budget for Project: ");
        Integer win = input.nextInt();
        input.nextLine();
        Integer loss = input.nextInt();
        input.nextLine();
        Integer rating = input.nextInt();
        input.nextLine();
        Integer country_id = input.nextInt();
        input.nextLine();
        TeamEntity entity = new TeamEntity(id, name, win, loss, rating, country_id);

        TeamService projectService = new TeamService();
        int count = projectService.update(entity);
        System.out.printf("There are updated %d rows\n", count);
    }

    private void deleteFromTeam() throws SQLException {
        System.out.println("Input ID(project_no) for Project: ");
        String id = input.nextLine();
        TeamService projectService = new TeamService();
        int count = projectService.delete(id);
        System.out.printf("There are deleted %d rows\n", count);
    }

    private void createForTeam() throws SQLException {
        System.out.println("Input ID(project_no) for Project: ");
        String id = input.nextLine();
        System.out.println("Input project_name for Project: ");
        String name = input.nextLine();
        System.out.println("Input budget for Project: ");
        Integer win = input.nextInt();
        input.nextLine();
        Integer loss = input.nextInt();
        input.nextLine();
        Integer rating = input.nextInt();
        input.nextLine();
        Integer country_id = input.nextInt();
        input.nextLine();
        TeamEntity entity = new TeamEntity(id, name, win, loss, rating, country_id);

        TeamService projectService = new TeamService();
        int count = projectService.create(entity);
        System.out.printf("There are created %d rows\n", count);
    }

    private void selectTeam() throws SQLException {
        System.out.println("\nTable: Project");
        TeamService projectService = new TeamService();
        List<TeamEntity> projects = projectService.findAll();
        for (TeamEntity entity : projects) {
            System.out.println(entity);
        }
    }

    private void findTeamByID() throws SQLException {
        System.out.println("Input ID(emp_no) for Employee: ");
        String id = input.nextLine();
        TeamService projectService = new TeamService();
        TeamEntity entity = projectService.findById(id);
        System.out.println(entity);
    }

    //------------------------------------------------------------------------

    private void deleteFromTeamHasPlayer() throws SQLException {
        System.out.println("Input ID(emp_no) for Works_on: ");
        Integer id_emp = input.nextInt();
        input.nextLine();
        System.out.println("Input ID(project_no) for Works_on: ");
        Integer id_project = input.nextInt();
        com.deyneka.model.TeamHasPlayer pk = new com.deyneka.model.TeamHasPlayer(id_emp, id_project);

        TeamHasPlayerService workOnService = new TeamHasPlayerService();
        int count = workOnService.delete(pk);
        System.out.printf("There are deleted %d rows\n", count);
    }

    private void createForTeamHasPlayer() throws SQLException {
        System.out.println("Input ID(emp_no) for Works_on: ");
        Integer id_emp = input.nextInt();
        input.nextLine();
        System.out.println("Input ID(project_no) for Works_on: ");
        Integer id_project = input.nextInt();
        com.deyneka.model.TeamHasPlayer pk = new com.deyneka.model.TeamHasPlayer(id_emp, id_project);

        System.out.println("Input job for Works_on: ");
        String job = input.nextLine();

        System.out.printf("There are created %d rows\n", job);
    }


    private void selectTeamHasPlayer() throws SQLException {
        System.out.println("\nTable: Works_on");
        TeamHasPlayerService workOnService = new TeamHasPlayerService();
        List<com.deyneka.model.TeamHasPlayer> works = workOnService.findAll();
        for (com.deyneka.model.TeamHasPlayer entity : works) {
            System.out.println(entity);
        }
    }

    //-------------------------------------------------------------------------

    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String key : menu.keySet())
            if (key.length() == 1) System.out.println(menu.get(key));
    }

    private void outputSubMenu(String fig) {

        System.out.println("\nSubMENU:");
        for (String key : menu.keySet())
            if (key.length() != 1 && key.substring(0, 1).equals(fig)) System.out.println(menu.get(key));
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point:");
            keyMenu = input.nextLine().toUpperCase();

            if (keyMenu.matches("^\\d")) {
                outputSubMenu(keyMenu);
                System.out.println("Please, select menu point:");
                keyMenu = input.nextLine().toUpperCase();
            }

            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }
}
