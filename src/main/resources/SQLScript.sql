-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema jdbc_test_db
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema jdbc_test_db
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `jdbc_test_db` DEFAULT CHARACTER SET utf8 ;
USE `jdbc_test_db` ;

-- -----------------------------------------------------
-- Table `jdbc_test_db`.`country`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `jdbc_test_db`.`country` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `jdbc_test_db`.`team`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `jdbc_test_db`.`team` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `win` INT NOT NULL,
  `loss` INT NOT NULL,
  `rating` DOUBLE NOT NULL,
  `country_id` INT NOT NULL,
  PRIMARY KEY (`id`, `country_id`),
  INDEX `fk_team_country1_idx` (`country_id` ASC) VISIBLE,
  CONSTRAINT `fk_team_country1`
    FOREIGN KEY (`country_id`)
    REFERENCES `jdbc_test_db`.`country` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `jdbc_test_db`.`player`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `jdbc_test_db`.`player` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `full_name` VARCHAR(45) NOT NULL,
  `year_of_birth` YEAR(4) NOT NULL,
  `uniform_id` INT NOT NULL,
  PRIMARY KEY (`id`, `uniform_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `jdbc_test_db`.`team_has_player`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `jdbc_test_db`.`team_has_player` (
  `team_id` INT NOT NULL,
  `player_id` INT NOT NULL,
  PRIMARY KEY (`team_id`, `player_id`),
  INDEX `fk_team_has_player_player1_idx` (`player_id` ASC) VISIBLE,
  INDEX `fk_team_has_player_team_idx` (`team_id` ASC) VISIBLE,
  CONSTRAINT `fk_team_has_player_team`
    FOREIGN KEY (`team_id`)
    REFERENCES `jdbc_test_db`.`team` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_team_has_player_player1`
    FOREIGN KEY (`player_id`)
    REFERENCES `jdbc_test_db`.`player` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;


insert into country values (1, 'Ukraine');
insert into country values (2, 'England');
insert into country values (3, 'USA');
insert into country values (4, 'China');
insert into country values (5, 'India');

insert into player values (1, 'Alex Dir', 1998, 2);
insert into player values (2, 'Serg Ole', 1990, 4);
insert into player values (3, 'Frank Sinatra', 1950, 1);
insert into player values (4, 'Big Foot', 1991, 2);
insert into player values (5, 'Nick Nelson', 1990, 1);
insert into player values (6, 'Brook Zero', 2000, 4);
insert into player values (7, 'Mickael Zoom', 1998, 4);
insert into player values (8, 'Gabriel Orlando', 1995, 1);

insert into team values (1, 'Karpaty', 15, 5, 10.1, 1);
insert into team values (2, 'Manchester United', 35, 4, 50.0, 3);
insert into team values (3, 'Real', 45, 12, 28.5, 4);
insert into team values (4, 'Juventus', 34, 1, 55.7, 2);
insert into team values (5, 'Dynamo', 25, 14, 18.9, 1);
insert into team values (6, 'Meteor', 11, 64, 6.0, 2);

insert into team_has_player values (3, 1);
insert into team_has_player values (2, 2);
insert into team_has_player values (1, 3);
insert into team_has_player values (4, 4);
insert into team_has_player values (5, 5);
insert into team_has_player values (6, 6);
insert into team_has_player values (1, 7);
insert into team_has_player values (2, 8);
insert into team_has_player values (1, 4);
insert into team_has_player values (4, 8);
